package shoptask.model;

import java.util.Comparator;

public class SportEquipment implements Comparator<SportEquipment>{
	private int id;
	private Category category;
	private String title;
	private int price;

	public SportEquipment() {
	}

	public SportEquipment(int id, Category category, String title, int price) {
		this.id = id;
		this.category = category;
		this.title = title;
		this.price = price;
	}

	
	@Override
	public int compare(SportEquipment o1, SportEquipment o2) {
		
		return o1.getTitle().compareTo(o2.getTitle());
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + price;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SportEquipment other = (SportEquipment) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (price != other.price)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("category=%-10s title=%-10s price=%-10d id=%d", getCategory().getCategoryName(), getTitle(),
				getPrice(),getId());
	}

	

}
